extern crate rand;

/// A module to encapsulate the different character classes which are used as
/// a base for password generation. Several functions provide a random
/// character from each base character class. These are used by the template
/// functions to construct passwords, passphrases or pins.
pub mod character_classes {
    use rand::prelude::*;

    pub const ALPHABETIC_UPPER_CASE: [&'static str; 26] = [
        "A", "E", "I", "O", "U", "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q",
        "R", "S", "T", "V", "W", "X", "Y", "Z",
    ];
    pub const ALPHABETIC_MIXED_CASE: [&'static str; 52] = [
        "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "B", "C", "D", "F", "G", "H", "J", "K",
        "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z", "b", "c", "d", "f", "g",
        "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z",
    ];
    pub const CONSONANTS_UPPER_CASE: [&'static str; 21] = [
        "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W",
        "X", "Y", "Z",
    ];
    pub const CONSONANTS_LOWER_CASE: [&'static str; 21] = [
        "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w",
        "x", "y", "z",
    ];
    pub const NUMERIC: [&'static str; 9] = ["1", "2", "3", "4", "5", "6", "7", "8", "9"];
    pub const OTHER: [&'static str; 24] = [
        "@", "&", "%", "?", ",", "=", "[", "]", "_", ":", "-", "+", "*", "$", "#", "!", "'", "^",
        "~", ";", "(", ")", "/", ".",
    ];
    pub const UNION_SET: [&'static str; 72] = [
        "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "B", "C", "D", "F", "G", "H", "J", "K",
        "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z", "b", "c", "d", "f", "g",
        "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z", "0", "1",
        "2", "3", "4", "5", "6", "7", "8", "9", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")",
    ];
    pub const VOWELS_UPPER_CASE: [&'static str; 5] = ["A", "E", "I", "O", "U"];
    pub const VOWELS_LOWER_CASE: [&'static str; 5] = ["a", "e", "i", "o", "u"];

    /// Return a single character from the list of uppercase alphabetic characters.
    /// The character is chosen randomly.
    pub fn A() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..ALPHABETIC_UPPER_CASE.len());
        ALPHABETIC_UPPER_CASE[e]
    }

    /// Return a single character from the list of mixed alphabetic characters.
    /// The character is chosen randomly.
    pub fn a() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..ALPHABETIC_MIXED_CASE.len());
        ALPHABETIC_MIXED_CASE[e]
    }

    /// Return a single character from the list of uppercase consonants.
    /// The character is chosen randomly.
    pub fn C() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..CONSONANTS_UPPER_CASE.len());
        CONSONANTS_UPPER_CASE[e]
    }

    /// Return a single character from the list of lowercase consonants.
    /// The character is chosen randomly.
    pub fn c() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..CONSONANTS_LOWER_CASE.len());
        CONSONANTS_LOWER_CASE[e]
    }

    /// Return a single character from the list of numbers.
    /// The character is chosen randomly.
    pub fn n() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..NUMERIC.len());
        NUMERIC[e]
    }

    /// Return a single character from the list of other characters.
    /// The character is chosen randomly.
    pub fn o() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..OTHER.len());
        OTHER[e]
    }

    /// Return a single character from the list of uppercase vowels.
    /// The character is chosen randomly.
    pub fn V() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..VOWELS_UPPER_CASE.len());
        VOWELS_UPPER_CASE[e]
    }

    /// Return a single character from the list of lowercase vowels.
    /// The character is chosen randomly.
    pub fn v() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..VOWELS_LOWER_CASE.len());
        VOWELS_LOWER_CASE[e]
    }

    /// Return a single character from the list of the union set.
    /// The character is chosen randomly.
    pub fn x() -> &'static str {
        let mut rng = rand::thread_rng();
        let e: usize = rng.gen_range(0..UNION_SET.len());
        UNION_SET[e]
    }
}

/// Template functions which return passwords, phrases and pins in different
/// formats which try to adhere to the security policies implemented by site
/// and account administrators. As these are not standardized this is can only
/// be a best-effort attempts at generating passwords which conform to these
/// policies.
pub mod templates {
    use super::character_classes::*;
    use rand::prelude::*;

    /// Generate a basic authentication information using alphabetic characters
    /// and numbers.
    pub fn basic() -> String {
        let template = rand::thread_rng().gen_range(1..=3);
        match template {
            1 => format!("{}{}{}{}{}{}{}{}", a(), a(), a(), n(), a(), a(), a(), n()),
            2 => format!("{}{}{}{}{}{}{}{}", a(), a(), n(), n(), a(), a(), a(), n()),
            3 => format!("{}{}{}{}{}{}{}{}", a(), a(), a(), n(), n(), a(), a(), a()),
            _ => panic!("Illegal template number!"),
        }
    }

    /// Generate a *long* authentication information (14 characters) using
    /// uppercase and lowercase characters, special characters, vowels and
    /// numbers.
    pub fn long() -> String {
        let template = rand::thread_rng().gen_range(1..=21);
        match template {
            1 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v()
            ),
            2 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o()
            ),
            3 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v()
            ),
            4 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v()
            ),
            5 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o()
            ),
            6 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v()
            ),
            7 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v()
            ),
            8 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o()
            ),
            9 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v()
            ),
            10 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                c()
            ),
            11 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o()
            ),
            12 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c()
            ),
            13 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v()
            ),
            14 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o()
            ),
            15 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v()
            ),
            16 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c()
            ),
            17 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o()
            ),
            18 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c()
            ),
            19 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c()
            ),
            20 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                c(),
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                c(),
                n(),
                o()
            ),
            21 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                C(),
                v(),
                c(),
                v(),
                C(),
                v(),
                c(),
                v(),
                n(),
                o(),
                C(),
                v(),
                c(),
                c()
            ),
            _ => panic!("Illegal template number!"),
        }
    }

    /// Generate a very secure authentication information using the union set
    /// of characters but try to include alphabetic and special characters as
    /// well as numbers in certain places to reflect common password policies.
    pub fn maximum_security() -> String {
        let template = rand::thread_rng().gen_range(1..=2);
        match template {
            1 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                a(),
                n(),
                o(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x()
            ),
            2 => format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}",
                a(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                x(),
                n(),
                o()
            ),
            _ => panic!("Illegal template number!"),
        }
    }

    /// Generate a medium sized authentication information using uppercase and
    /// lowercase characters, vowels and numbers and special characters.
    pub fn medium() -> String {
        let template = rand::thread_rng().gen_range(1..=2);
        match template {
            1 => format!("{}{}{}{}{}{}{}{}", C(), v(), c(), n(), o(), C(), v(), c()),
            2 => format!("{}{}{}{}{}{}{}{}", C(), v(), c(), C(), v(), c(), n(), o()),
            _ => panic!("Illegal template number!"),
        }
    }

    /// Generate an authentication information which would pass as a "name"
    /// using characters and vowels (all lowercase).
    pub fn name() -> String {
        format!(
            "{}{}{}{}{}{}{}{}{}",
            c(),
            v(),
            c(),
            c(),
            v(),
            c(),
            v(),
            c(),
            v()
        )
    }

    /// Generate a PIN with 4 digits.
    pub fn pin() -> String {
        format!("{}{}{}{}", n(), n(), n(), n())
    }

    /// Generate a PIN with 5 digits.
    pub fn pin_5() -> String {
        format!("{}{}{}{}{}", n(), n(), n(), n(), n())
    }

    /// Generate a PIN with 6 digits.
    pub fn pin_6() -> String {
        format!("{}{}{}{}{}{}", n(), n(), n(), n(), n(), n())
    }

    /// Generate a passphrase using lowercase characters and vowels splitted
    /// into several "words".
    pub fn phrase() -> String {
        let template = rand::thread_rng().gen_range(1..=3);
        match template {
            1 => format!(
                "{}{}{}{} {}{}{} {}{}{}{}{}{}{} {}{}{}",
                c(),
                v(),
                c(),
                c(),
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                v(),
                c(),
                v(),
                c()
            ),
            2 => format!(
                "{}{}{} {}{}{}{}{}{}{}{}{} {}{}{}{}",
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                v(),
                c(),
                v(),
                c(),
                v(),
                c(),
                v()
            ),
            3 => format!(
                "{}{} {}{}{}{}{} {}{}{} {}{}{}{}{}{}{}",
                c(),
                v(),
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                v(),
                c(),
                c(),
                v(),
                c(),
                v(),
                c(),
                c(),
                v()
            ),
            _ => panic!("Illegal template number!"),
        }
    }

    /// Generate a very short authentication information using uppercase and
    /// lowercase characters as well as at least one vowel and a number.
    pub fn short() -> String {
        format!("{}{}{}{}", C(), v(), c(), n())
    }
}

///
/// Unit Tests
///
#[cfg(test)]
extern crate quickcheck;
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;

#[cfg(test)]
mod tests {
    use super::character_classes::*;

    #[test]
    fn rand_A() {
        let gen = &A();
        assert!(ALPHABETIC_UPPER_CASE.contains(gen));
    }

    #[test]
    fn rand_a() {
        let gen = &a();
        assert!(ALPHABETIC_MIXED_CASE.contains(gen));
    }

    #[test]
    fn rand_C() {
        let gen = &C();
        assert!(CONSONANTS_UPPER_CASE.contains(gen));
    }

    #[test]
    fn rand_c() {
        let gen = &c();
        assert!(CONSONANTS_LOWER_CASE.contains(gen));
    }

    #[test]
    fn rand_n() {
        let gen = &n();
        assert!(NUMERIC.contains(gen));
    }

    #[test]
    fn rand_o() {
        let gen = &o();
        assert!(OTHER.contains(gen));
    }

    #[test]
    fn rand_V() {
        let gen = &V();
        assert!(VOWELS_UPPER_CASE.contains(gen));
    }

    #[test]
    fn rand_v() {
        let gen = &v();
        assert!(VOWELS_LOWER_CASE.contains(gen));
    }

    #[test]
    fn rand_x() {
        let gen = &x();
        assert!(UNION_SET.contains(gen));
    }
}
