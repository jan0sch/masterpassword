extern crate clap;

use clap::{App, Arg};
use passwordghost::*;
use std::io;
use termion::raw::IntoRawMode;
use tui::backend::TermionBackend;
use tui::Terminal;

fn main() -> Result<(), io::Error> {
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let app = App::new("Password Ghost")
        .version("0.1.0")
        .about("Password application for the command line, based on the masterpassword algorithm.")
        .arg(Arg::with_name("name").help("Your name e.g. main identification."))
        .arg(
            Arg::with_name("site-name")
                .help("The site name e.g. domain you want to generate the password for."),
        )
        .arg(
            Arg::with_name("counter")
                .help("The counter for the password generator which defaults to 1."),
        );
    Ok(())
}
